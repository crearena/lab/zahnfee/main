import { writable } from 'svelte/store'
import type { UserInfo } from 'firebase/auth'

const {subscribe, set} = writable<{
    isLoggedIn: boolean
    user?: UserInfo
}>({ isLoggedIn: true })

export default { subscribe, set }