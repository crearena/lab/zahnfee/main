
interface Dentist {
    id: string
    name: string
    bio: string
    rating: string
    location: [number, number]
    reviews: number
}

interface Appointment {
    id: string
    timestamp: Date
}

interface Issues {
    id: string
    description: string
}