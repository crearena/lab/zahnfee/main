import FirebaseConfig from '@/config/firebase'
import type {FirebaseApp} from 'firebase/app'

export async function init():Promise<FirebaseApp> {
    const {initializeApp, getApps} = await import("firebase/app")
    if (getApps().length === 0 ) {
         return initializeApp(FirebaseConfig)
    }
}
