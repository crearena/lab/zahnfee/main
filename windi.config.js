export default {
	darkMode: false,
	theme: {
		fontFamily: {
			sans: ['Open Sans', 'sans-serif']
		},
		extend: {
		}
	},
	variants: {
		extend: {}
	},
	plugins: [
	]
}
