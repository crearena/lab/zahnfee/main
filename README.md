## Web App

This is the prototype app to help users find dentists near them it allows the user to:

- Create an account 
- Login to an account 
- Rotate the 3D teeth model 
- Highlight sick teeth 
- Show information about the sick tooth
- Recommend dentists near the user in a list/map view 
- allow the user to book an appointment 

## Stack
- [SvelteKit](https://kit.svelte.dev)
- [WindiCSS](https://windicss.org)
- [TypeScript](https://typescriptlang.org)
- [Firebase](https://firebase.com)

## Commands  

After installing using `npm install` the following comands are  available 

```bash
npm run dev #run the development server 
```

```bash
npm run build #generate the production build
```

```bash
npm run build && firebase deploy #deploy to firebase
```
